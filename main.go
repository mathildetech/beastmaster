/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"errors"
	"flag"
	"os"
	"strings"
	"time"

	beastv1 "bitbucket.org/mathildetech/beastmaster/api/v1"
	"bitbucket.org/mathildetech/beastmaster/controllers"
	"k8s.io/apimachinery/pkg/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
	clusterapi "k8s.io/cluster-registry/pkg/apis/clusterregistry/v1alpha1"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"
	// +kubebuilder:scaffold:imports
)

var (
	scheme   = runtime.NewScheme()
	setupLog = ctrl.Log.WithName("setup")
)

func init() {
	_ = clientgoscheme.AddToScheme(scheme)

	_ = beastv1.AddToScheme(scheme)

	_ = clusterapi.AddToScheme(scheme)

	// +kubebuilder:scaffold:scheme
}

func main() {
	var metricsAddr string
	var enableLeaderElection bool
	var enableControllersStr string
	var requestTimeoutTmp int64
	flag.StringVar(&metricsAddr, "metrics-addr", ":8089", "The address the metric endpoint binds to.")
	flag.BoolVar(&enableLeaderElection, "enable-leader-election", false,
		"Enable leader election for controller manager. Enabling this will ensure there is only one active controller manager.")
	flag.StringVar(&enableControllersStr, "enable-controllers", controllers.GetEnableControllers(),
		"config the type of source occurred in enableControllers string split by comma be controller and sync")
	flag.StringVar(&controllers.CustomFinalizerName, "custom-finalizer-name", controllers.DefaultCustomFinalizerName,
		"the name of custom controllers finalizer name, but exclude the node controllers.")
	flag.StringVar(&controllers.StandardFinalizerName, "standard-finalizer-name", controllers.DefaultStandardFinalizerName,
		"the name of standard controllers finalizer name for controllers like node controller")
	flag.BoolVar(&controllers.StopSyncMode, "stop-sync-mode-enable", controllers.DefaultStopSyncMode,
		"before remove this pod from cluster, you need setting stop-sync-mode-enable to true, so that this pod"+
			" will remove all finalizers from watched object.")
	flag.StringVar(&controllers.LocateClusterName, "locate-cluster-name", "",
		"cluster-name to be used identify the resources locate cluster, can't ot be empty!")
	flag.StringVar(&controllers.HostPort, "host-port", controllers.DefaultHostPort,
		"the adaptor's host and port, default is 127.0.0.1:8082")
	flag.Int64Var(&requestTimeoutTmp, "request-timeout", 5000, "sync source server http time duration,"+
		" the default value equal 5000 milliseconds")
	flag.Parse()
	controllers.InitParameter()

	controllers.RequestTimeout = time.Duration(requestTimeoutTmp) * time.Millisecond

	ctrl.SetLogger(zap.New(func(o *zap.Options) {
		o.Development = true
	}))

	mgr, err := ctrl.NewManager(ctrl.GetConfigOrDie(), ctrl.Options{
		Scheme:             scheme,
		MetricsBindAddress: metricsAddr,
		LeaderElection:     enableLeaderElection,
		Port:               9443,
	})
	if err != nil {
		setupLog.Error(err, "unable to start manager")
		os.Exit(1)
	}

	if controllers.LocateClusterName == "" {
		err := errors.New("cluster-name can't be empty!")
		setupLog.Error(err, "problem running manager")
		os.Exit(1)
	}

	setupLog.Info("", "enableControllersStr", enableControllersStr)
	setupLog.Info("", "CustomFinalizerName", controllers.CustomFinalizerName)
	setupLog.Info("", "StandardFinalizerName", controllers.StandardFinalizerName)
	enableControllers := strings.Split(enableControllersStr, ",")

	if controllers.ContainsString(enableControllers, controllers.ProjectKind) {
		if err = (&controllers.ProjectReconciler{
			Client: mgr.GetClient(),
			Log:    ctrl.Log.WithName("controllers").WithName("Project"),
			Scheme: mgr.GetScheme(),
		}).SetupWithManager(mgr); err != nil {
			setupLog.Error(err, "unable to create controller", "controller", "Project")
			os.Exit(1)
		}
	}

	if controllers.ContainsString(enableControllers, controllers.UserBindingKind) {
		if err = (&controllers.UserBindingReconciler{
			Client: mgr.GetClient(),
			Log:    ctrl.Log.WithName("controllers").WithName("UserBinding"),
			Scheme: mgr.GetScheme(),
		}).SetupWithManager(mgr); err != nil {
			setupLog.Error(err, "unable to create controller", "controller", "UserBinding")
			os.Exit(1)
		}
	}

	if controllers.ContainsString(enableControllers, controllers.NodeKind) {
		if err = (&controllers.NodeReconciler{
			Client: mgr.GetClient(),
			Log:    ctrl.Log.WithName("controllers").WithName("Node"),
			Scheme: mgr.GetScheme(),
		}).SetupWithManager(mgr); err != nil {
			setupLog.Error(err, "unable to create controller", "controller", "Node")
			os.Exit(1)
		}
	}

	if controllers.ContainsString(enableControllers, controllers.ClusterKind) {
		if err = (&controllers.ClusterReconciler{
			Client: mgr.GetClient(),
			Log:    ctrl.Log.WithName("controllers").WithName("Cluster"),
			Scheme: mgr.GetScheme(),
		}).SetupWithManager(mgr); err != nil {
			setupLog.Error(err, "unable to create controller", "controller", "Cluster")
			os.Exit(1)
		}
	}

	if controllers.ContainsString(enableControllers, controllers.NamespaceKind) {
		if err = (&controllers.NamespaceReconciler{
			Client: mgr.GetClient(),
			Log:    ctrl.Log.WithName("controllers").WithName("Namespace"),
			Scheme: mgr.GetScheme(),
		}).SetupWithManager(mgr); err != nil {
			setupLog.Error(err, "unable to create controller", "controller", "Namespace")
			os.Exit(1)
		}
	}

	// +kubebuilder:scaffold:builder

	setupLog.Info("starting manager")
	if err := mgr.Start(ctrl.SetupSignalHandler()); err != nil {
		setupLog.Error(err, "problem running manager")
		os.Exit(1)
	}
}
