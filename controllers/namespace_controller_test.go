package controllers

import (
	"k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/log"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"net/http"
	"net/http/httptest"
)

var _ = Describe("NamespaceController", func() {

	const timeout = time.Second * 30
	const interval = time.Second * 1
	var Namespace *v1.Namespace
	var key types.NamespacedName

	BeforeEach(func() {

		logNamespace = log.Log.WithValues("Namespace", "")

		spec := v1.NamespaceSpec{}

		key = types.NamespacedName{
			Name: "aaa",
		}

		Namespace = &v1.Namespace{
			ObjectMeta: metav1.ObjectMeta{
				Name: key.Name,
				Labels: map[string]string{
					"alauda.io/project": "aaa",
				},
				Annotations: map[string]string{
					"ovn.kubernetes.io/exclude_ips":    "10.199.0.1",
					"ovn.kubernetes.io/logical_switch": "ovn-default",
				},
			},
			Spec: spec,
		}
	})

	Describe("send data to server", func() {
		Context("test response", func() {

			It("response statusCode should equal 200", func() {
				ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(200)
				}))
				defer ts.Close()

				httpCode, _ := SendNamespaceData(Namespace, http.MethodPut, ts.URL)
				Expect(httpCode).To(Equal(200))
			})

			It("response statusCode should equal 404", func() {
				ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(404)
				}))
				defer ts.Close()

				httpCode, _ := SendNamespaceData(Namespace, http.MethodPut, ts.URL)
				Expect(httpCode).To(Equal(404))
			})

			It("response statusCode should equal 500", func() {
				ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(500)
				}))
				defer ts.Close()

				httpCode, _ := SendNamespaceData(Namespace, http.MethodPut, ts.URL)
				Expect(httpCode).To(Equal(500))
			})
		})

		Context("tests different http method", func() {
			It("test with UPDATE method", func() {
				ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(200)
				}))
				defer ts.Close()

				httpCode, _ := SendNamespaceData(Namespace, http.MethodPut, ts.URL)
				Expect(httpCode).To(Equal(200))

			})

			It("test with DELETE method", func() {
				ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(200)
				}))
				defer ts.Close()

				httpCode, _ := SendNamespaceData(Namespace, http.MethodDelete, ts.URL)
				Expect(httpCode).To(Equal(200))

			})
		})

		Context("tests GetHttpMethod", func() {
			It("update event return PUT", func() {
				Expect(GetHttpMethod(false)).To(Equal(http.MethodPut))
			})

			It("create event return PUT", func() {
				Expect(GetHttpMethod(true)).To(Equal(http.MethodDelete))
			})

		})

	})

})
