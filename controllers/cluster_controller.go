/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"github.com/go-logr/logr"
	"github.com/google/uuid"
	"k8s.io/apimachinery/pkg/runtime"
	clusterapi "k8s.io/cluster-registry/pkg/apis/clusterregistry/v1alpha1"
	"net/http"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

// ClusterReconciler reconciles a Cluster object
type ClusterReconciler struct {
	client.Client
	Log    logr.Logger
	Scheme *runtime.Scheme
}

type ClusterReqBody struct {
	Cluster *clusterapi.Cluster `json:"cluster"`
}

var logCluster logr.Logger

// +kubebuilder:rbac:groups=auth.alauda.io,resources=Clusters,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=auth.alauda.io,resources=Clusters/status,verbs=get;update;patch

func (r *ClusterReconciler) Reconcile(req ctrl.Request) (ctrl.Result, error) {
	_ = context.Background()
	logCluster = r.Log.WithValues("Cluster", req.NamespacedName)

	// your logic here
	cluster := &clusterapi.Cluster{}
	err := r.Get(context.Background(), req.NamespacedName, cluster)
	if err != nil {
		logCluster.Error(err, "GetCluster Error")
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	if StopSyncMode {
		err := RemoveClusterFinalizer(r, cluster)
		return ctrl.Result{}, err
	}

	isDeleting := false
	if cluster.ObjectMeta.DeletionTimestamp.IsZero() {
		if !ContainsString(cluster.ObjectMeta.Finalizers, CustomFinalizerName) {
			cluster.ObjectMeta.Finalizers = append(cluster.ObjectMeta.Finalizers, CustomFinalizerName)
			err = r.Update(context.Background(), cluster)
			return ctrl.Result{}, err
		}
	} else {
		isDeleting = true
	}

	httpMethod := GetHttpMethod(isDeleting)
	statusCode, err := SendClusterData(cluster, httpMethod, ClusterUrl)
	if err != nil {
		logCluster.Error(err, "SendClusterData Error")
		return ctrl.Result{}, err
	}

	if statusCode >= http.StatusInternalServerError {
		logCluster.Error(nil, "SendClusterData Error", "statusCode ", statusCode)
		return ctrl.Result{}, fmt.Errorf("internal error")
	}

	// remove finalizers for watched object if isDeleting equal true
	if isDeleting {
		err := RemoveClusterFinalizer(r, cluster)
		return ctrl.Result{}, err
	}

	return ctrl.Result{}, nil
}

func (r *ClusterReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&clusterapi.Cluster{}).Complete(r)
}

// send Cluster data to sync server
func SendClusterData(Cluster *clusterapi.Cluster, httpMethod, url string) (httpCode int, err error) {
	ClusterReqBody := ClusterReqBody{Cluster: Cluster.DeepCopy()}
	body, _ := json.Marshal(ClusterReqBody)
	req, err := http.NewRequest(httpMethod, url, bytes.NewBuffer(body))
	if err != nil {
		return 0, nil
	}
	logCluster.Info("SendClusterData", "body:", string(body))
	requestId := uuid.New().String()
	req.Header.Set("request_id", requestId)
	req.Header.Set("Content-Type", "application/json")

	httpClient := &http.Client{Timeout: RequestTimeout}
	res, err := httpClient.Do(req)
	if err != nil {
		return 0, err
	}

	logCluster.Info("SendClusterData",
		"StatusCode", res.StatusCode,
		"request_id", requestId,
		"httpMethod", httpMethod,
		"Cluster.name", Cluster.Name)

	return res.StatusCode, nil
}

// remove finalizers
func RemoveClusterFinalizer(r *ClusterReconciler, cluster *clusterapi.Cluster) error {
	if ContainsString(cluster.ObjectMeta.Finalizers, CustomFinalizerName) {
		cluster.ObjectMeta.Finalizers = removeString(cluster.ObjectMeta.Finalizers, CustomFinalizerName)
		err := r.Update(context.Background(), cluster)
		if err == nil {
			logCluster.Info(RemoveFinalizerSuccessMessage)
		}
		return err
	}
	return nil
}
