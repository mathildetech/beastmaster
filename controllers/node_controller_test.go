package controllers

import (
	"context"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/log"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"net/http"
	"net/http/httptest"
)

var _ = Describe("NodeController", func() {

	const timeout = time.Second * 30
	const interval = time.Second * 1
	var Node *corev1.Node

	BeforeEach(func() {

		logNode = log.Log.WithValues("Node", "")

		spec := corev1.NodeSpec{
			PodCIDR:  "10.199.0.0/24",
			PodCIDRs: []string{"10.199.0.0/24"},
			Taints: []corev1.Taint{{
				Effect: "NoSchedule",
				Key:    "node.kubernetes.io/unreachable",
			}},
		}

		key := types.NamespacedName{
			Name:      "int-master-1",
			Namespace: "default",
		}

		Node = &corev1.Node{
			ObjectMeta: metav1.ObjectMeta{
				Name:      key.Name,
				Namespace: key.Namespace,
				Labels: map[string]string{
					"beta.kubernetes.io/arch":        "amd64",
					"beta.kubernetes.io/os":          "linux",
					"global":                         "true",
					"ingress":                        "true",
					"ip":                             "192.168.17.2",
					"kube-ovn/role":                  "master",
					"kubernetes.io/arch":             "amd64",
					"kubernetes.io/hostname":         "int-master-1",
					"kubernetes.io/os":               "linux",
					"log":                            "true",
					"monitoring":                     "enabled",
					"node-role.kubernetes.io/master": "",
				},
				Annotations: map[string]string{
					"node.alpha.kubernetes.io/ttl":     "0",
					"ovn.kubernetes.io/cidr":           "100.64.0.0/16",
					"ovn.kubernetes.io/gateway":        "100.64.0.1",
					"ovn.kubernetes.io/ip_address":     "100.64.0.4",
					"ovn.kubernetes.io/logical_switch": "join",
					"ovn.kubernetes.io/mac_address":    "0a:00:00:40:00:05",
					"ovn.kubernetes.io/port_name":      "node-int-master-1",
				},
			},
			Spec: spec,
		}
	})

	Describe("send data to server", func() {
		Context("test response", func() {
			It("create a Node", func() {
				Expect(k8sClient.Create(context.Background(), Node)).Should(Succeed())
				key := types.NamespacedName{
					Name:      "int-master-1",
					Namespace: "default",
				}
				time.Sleep(time.Second * 1)
				fetched := &corev1.Node{}
				Eventually(func() string {
					err := k8sClient.Get(context.Background(), key, fetched)
					if err != nil {
						return ""
					}
					return fetched.Name
				}, timeout, interval).Should(Equal("int-master-1"))
			})

			It("response statusCode should equal 200", func() {
				ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(200)
				}))
				defer ts.Close()

				httpCode, _ := SendNodeData(Node, http.MethodPut, ts.URL)
				Expect(httpCode).To(Equal(200))
			})

			It("response statusCode should equal 404", func() {
				ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(404)
				}))
				defer ts.Close()

				httpCode, _ := SendNodeData(Node, http.MethodPut, ts.URL)
				Expect(httpCode).To(Equal(404))
			})

			It("response statusCode should equal 500", func() {
				ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(500)
				}))
				defer ts.Close()

				httpCode, _ := SendNodeData(Node, http.MethodPut, ts.URL)
				Expect(httpCode).To(Equal(500))
			})
		})

		Context("tests different http method", func() {
			It("test with UPDATE method", func() {
				ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(200)
				}))
				defer ts.Close()

				httpCode, _ := SendNodeData(Node, http.MethodPut, ts.URL)
				Expect(httpCode).To(Equal(200))

			})

			It("test with DELETE method", func() {
				ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(200)
				}))
				defer ts.Close()

				httpCode, _ := SendNodeData(Node, http.MethodDelete, ts.URL)
				Expect(httpCode).To(Equal(200))

			})
		})

		Context("tests GetHttpMethod", func() {
			It("update event return PUT", func() {
				Expect(GetHttpMethod(false)).To(Equal(http.MethodPut))
			})

			It("create event return PUT", func() {
				Expect(GetHttpMethod(true)).To(Equal(http.MethodDelete))
			})

		})

	})

})
