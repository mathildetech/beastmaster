/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	authv1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"github.com/go-logr/logr"
	"github.com/google/uuid"
	"k8s.io/apimachinery/pkg/runtime"
	"net/http"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

// ProjectReconciler reconciles a Project object
type ProjectReconciler struct {
	client.Client
	Log    logr.Logger
	Scheme *runtime.Scheme
}

type ProjectReqBody struct {
	Project *authv1.Project `json:"project"`
}

var logProject logr.Logger

// +kubebuilder:rbac:groups=auth.alauda.io,resources=projects,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=auth.alauda.io,resources=projects/status,verbs=get;update;patch

func (r *ProjectReconciler) Reconcile(req ctrl.Request) (ctrl.Result, error) {
	_ = context.Background()
	logProject = r.Log.WithValues("project", req.NamespacedName)

	// your logic here
	project := &authv1.Project{}
	err := r.Get(context.Background(), req.NamespacedName, project)
	if err != nil {
		logProject.Error(err, "GetProject Error")
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	// StopSyncMode open, not sync resources
	if StopSyncMode {
		err := RemoveProjectFinalizer(r, project)
		return ctrl.Result{}, err
	}

	isDeleting := false
	if project.ObjectMeta.DeletionTimestamp.IsZero() {
		if !ContainsString(project.ObjectMeta.Finalizers, CustomFinalizerName) {
			project.ObjectMeta.Finalizers = append(project.ObjectMeta.Finalizers, CustomFinalizerName)
			err = r.Update(context.Background(), project)
			return ctrl.Result{}, err
		}
	} else {
		isDeleting = true
	}

	httpMethod := GetHttpMethod(isDeleting)
	statusCode, err := SendProjectData(project, httpMethod, ProjectUrl)
	if err != nil {
		logProject.Error(err, "SendProjectData Error")
		return ctrl.Result{}, err
	}

	if statusCode >= http.StatusInternalServerError {
		logProject.Error(nil, "SendProjectData Error", "statusCode ", statusCode)
		return ctrl.Result{}, fmt.Errorf("internal error")
	}

	// remove finalizers for watched object if isDeleting equal true
	if isDeleting {
		err := RemoveProjectFinalizer(r, project)
		return ctrl.Result{}, err
	}
	return ctrl.Result{}, nil
}

func (r *ProjectReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&authv1.Project{}).Complete(r)
}

// send Project data to sync server
func SendProjectData(project *authv1.Project, httpMethod, url string) (httpCode int, err error) {
	projectReqBody := ProjectReqBody{Project: project.DeepCopy()}
	body, err := json.Marshal(projectReqBody)
	if err != nil {
		logProject.Error(err, "error")
		return 0, err
	}
	logProject.Info("SendProjectData", "body:", string(body))
	req, err := http.NewRequest(httpMethod, url, bytes.NewBuffer(body))
	if err != nil {
		return 0, nil
	}
	requestId := uuid.New().String()
	req.Header.Set("request_id", requestId)
	req.Header.Set("Content-Type", "application/json")

	httpClient := &http.Client{Timeout: RequestTimeout}
	res, err := httpClient.Do(req)
	if err != nil {
		return 0, err
	}

	logProject.Info("SendProjectData",
		"StatusCode", res.StatusCode,
		"request_id", requestId,
		"httpMethod", httpMethod,
		"project.name", project.Name)

	return res.StatusCode, nil
}

// get http method when watch object is deleting
func GetHttpMethod(isDeleting bool) (method string) {
	if isDeleting {
		method = http.MethodDelete
	} else {
		method = http.MethodPut
	}
	return
}

// judge a string is contain string array
func ContainsString(slice []string, s string) bool {
	for _, item := range slice {
		if item == s {
			return true
		}
	}
	return false
}

// remove string from string array
func removeString(slice []string, s string) (result []string) {
	result = []string{}
	for _, item := range slice {
		if item == s {
			continue
		}
		result = append(result, item)
	}
	return
}

// remove finalizers
func RemoveProjectFinalizer(r *ProjectReconciler, project *authv1.Project) error {
	if ContainsString(project.ObjectMeta.Finalizers, CustomFinalizerName) {
		project.ObjectMeta.Finalizers = removeString(project.ObjectMeta.Finalizers, CustomFinalizerName)
		err := r.Update(context.Background(), project)
		if err == nil {
			logProject.Info(RemoveFinalizerSuccessMessage)
		}
		return err
	}
	return nil
}
