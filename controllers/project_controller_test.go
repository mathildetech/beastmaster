package controllers

import (
	authv1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1"
	"context"
	"encoding/json"
	"io/ioutil"
	"k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/log"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"net/http"
	"net/http/httptest"
)

var _ = Describe("ProjectController", func() {

	const timeout = time.Second * 30
	const interval = time.Second * 1
	var project *authv1.Project
	var key types.NamespacedName

	BeforeEach(func() {

		logProject = log.Log.WithValues("Project", "")

		spec := authv1.ProjectSpec{
			Clusters: []*authv1.ProjectClusters{
				{
					Name:  "global",
					Quota: v1.ResourceList{},
				},
			},
		}

		key = types.NamespacedName{
			Name:      "project-test-01",
			Namespace: "default",
		}

		project = &authv1.Project{
			ObjectMeta: metav1.ObjectMeta{
				Name:      key.Name,
				Namespace: key.Namespace,
				Labels: map[string]string{
					"alauda.io/project":        "aaa",
					"alauda.io/project.level":  "1",
					"alauda.io/project.parent": "",
				},
				Annotations: map[string]string{
					"alauda.io/creator":      "admin@cpaas.io",
					"alauda.io/description":  "werwerwer",
					"alauda.io/display-name": "项目显示名称rewrew",
				},
				Finalizers: []string{},
			},
			Spec: spec,
		}
	})

	Describe("send data to server", func() {
		Context("test response", func() {
			It("create a project", func() {
				Expect(k8sClient.Create(context.Background(), project)).Should(Succeed())
				fetched := &authv1.Project{}
				Eventually(func() string {
					err := k8sClient.Get(context.Background(), key, fetched)
					if err != nil {
						return ""
					}
					return fetched.Name
				}, timeout, interval).Should(Equal("project-test-01"))
			})

			It("response statusCode should equal 200", func() {
				ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(200)
				}))
				defer ts.Close()

				httpCode, _ := SendProjectData(project, http.MethodPut, ts.URL)
				Expect(httpCode).To(Equal(200))
			})

			It("response statusCode should equal 404", func() {
				ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(404)
				}))
				defer ts.Close()

				httpCode, _ := SendProjectData(project, http.MethodPut, ts.URL)
				Expect(httpCode).To(Equal(404))
			})

			It("response statusCode should equal 500", func() {
				ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(500)
				}))
				defer ts.Close()

				httpCode, _ := SendProjectData(project, http.MethodPut, ts.URL)
				Expect(httpCode).To(Equal(500))
			})
		})

		Context("tests different http method", func() {
			It("test with UPDATE method", func() {
				ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(200)
				}))
				defer ts.Close()

				httpCode, _ := SendProjectData(project, http.MethodPut, ts.URL)
				Expect(httpCode).To(Equal(200))

			})

			It("test with DELETE method", func() {
				ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					p := new(ProjectReqBody)
					body, err := ioutil.ReadAll(r.Body)
					if err != nil {
						w.WriteHeader(500)
					}
					err = json.Unmarshal(body, &p)
					if err != nil {
						w.WriteHeader(501)
					}
					w.WriteHeader(200)
				}))
				defer ts.Close()

				httpCode, _ := SendProjectData(project, http.MethodDelete, ts.URL)
				Expect(httpCode).To(Equal(200))

			})
		})

		Context("tests GetHttpMethod", func() {
			It("update event return PUT", func() {
				Expect(GetHttpMethod(false)).To(Equal(http.MethodPut))
			})

			It("create event return PUT", func() {
				Expect(GetHttpMethod(true)).To(Equal(http.MethodDelete))
			})

		})

		Context("tests ContainsString", func() {
			It("have aaa equal true", func() {
				strings := []string{"bbb", "ccc", "123", "aaa", "eee"}
				Expect(ContainsString(strings, "aaa")).To(Equal(true))
			})

			It("have aaa equal false", func() {
				strings := []string{"bbb", "ccc", "123", "eee"}
				Expect(ContainsString(strings, "aaa")).To(Equal(false))
			})

		})

		Context("tests removeString", func() {
			It("remove aaa from strings", func() {
				strings := []string{"aaa"}
				want := make([]string, 0)
				Expect(removeString(strings, "aaa")).To(Equal(want))
			})
		})

		Context("tests remove finalizers from project", func() {
			It("return equal nil", func() {
				err := RemoveProjectFinalizer(projectReconciler, project)
				Expect(err).To(BeNil())
			})
		})

		Context("tests remove finalizers from project", func() {
			It("have finalizers and  return equal nil", func() {
				CustomFinalizerName = DefaultCustomFinalizerName
				fetched := &authv1.Project{}
				err := k8sClient.Get(context.Background(), key, fetched)
				Expect(err).To(BeNil())

				fetched.ObjectMeta.Finalizers = append(fetched.ObjectMeta.Finalizers, CustomFinalizerName)
				err = projectReconciler.Update(context.Background(), fetched)
				Expect(err).To(BeNil())
				Expect(fetched.ObjectMeta.Finalizers).To(Equal([]string{
					CustomFinalizerName}))

				err = RemoveProjectFinalizer(projectReconciler, fetched)
				Expect(fetched.ObjectMeta.Finalizers).To(Equal([]string{}))
				Expect(err).To(BeNil())
			})
		})
	})

})
