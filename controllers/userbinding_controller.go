/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	authv1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"github.com/go-logr/logr"
	"github.com/google/uuid"
	"k8s.io/apimachinery/pkg/runtime"
	"net/http"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

// UserBindingReconciler reconciles a UserBinding object
type UserBindingReconciler struct {
	client.Client
	Log    logr.Logger
	Scheme *runtime.Scheme
}

type UserBindingReqBody struct {
	UserBinding *authv1.UserBinding `json:"userbinding"`
}

var logUserBinding logr.Logger

// +kubebuilder:rbac:groups=auth.alauda.io,resources=userbindings,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=auth.alauda.io,resources=userbindings/status,verbs=get;update;patch

func (r *UserBindingReconciler) Reconcile(req ctrl.Request) (ctrl.Result, error) {
	_ = context.Background()
	logUserBinding = r.Log.WithValues("userbinding", req.NamespacedName)

	// your logic here
	userBinding := &authv1.UserBinding{}
	err := r.Get(context.Background(), req.NamespacedName, userBinding)
	if err != nil {
		logUserBinding.Error(err, "GetUserBinding Error")
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	if StopSyncMode {
		err := RemoveUserBindingFinalizer(r, userBinding)
		return ctrl.Result{}, err
	}

	isDeleting := false
	if userBinding.ObjectMeta.DeletionTimestamp.IsZero() {
		if !ContainsString(userBinding.ObjectMeta.Finalizers, CustomFinalizerName) {
			userBinding.ObjectMeta.Finalizers = append(userBinding.ObjectMeta.Finalizers, CustomFinalizerName)
			err = r.Update(context.Background(), userBinding)
			return ctrl.Result{}, err
		}
	} else {
		isDeleting = true
	}

	httpMethod := GetHttpMethod(isDeleting)
	statusCode, err := SendUserBindingData(userBinding, httpMethod, UserBindingUrl)
	if err != nil {
		logUserBinding.Error(err, "SendUserBindingData Error")
		return ctrl.Result{}, err
	}

	if statusCode >= http.StatusInternalServerError {
		logUserBinding.Error(nil, "SendUserBindingData Error", "statusCode:", statusCode)
		return ctrl.Result{}, fmt.Errorf("internal error")
	}

	// remove finalizers for watched object when isDeleting equal true
	if isDeleting {
		err := RemoveUserBindingFinalizer(r, userBinding)
		return ctrl.Result{}, err

	}

	return ctrl.Result{}, nil
}

func (r *UserBindingReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&authv1.UserBinding{}).Complete(r)
}

// send UserBinding data to sync server
func SendUserBindingData(userbinding *authv1.UserBinding, httpMethod, url string) (httpCode int, err error) {
	userBindingReqBody := UserBindingReqBody{UserBinding: userbinding.DeepCopy()}
	body, _ := json.Marshal(userBindingReqBody)
	req, err := http.NewRequest(httpMethod, url, bytes.NewBuffer(body))
	if err != nil {
		return 0, nil
	}
	logUserBinding.Info("SendUserBindingData", "body:", string(body))
	requestId := uuid.New().String()
	req.Header.Set("request_id", requestId)
	req.Header.Set("Content-Type", "application/json")

	httpClient := &http.Client{Timeout: RequestTimeout}
	res, err := httpClient.Do(req)
	if err != nil {
		return 0, err
	}

	logUserBinding.Info("SendUserBindingData",
		"StatusCode", res.StatusCode,
		"request_id", requestId,
		"httpMethod", httpMethod,
		"userbinding.name", userbinding.Name)

	return res.StatusCode, nil
}

// remove finalizers
func RemoveUserBindingFinalizer(r *UserBindingReconciler, userBinding *authv1.UserBinding) error {
	if ContainsString(userBinding.ObjectMeta.Finalizers, CustomFinalizerName) {
		userBinding.ObjectMeta.Finalizers = removeString(userBinding.ObjectMeta.Finalizers, CustomFinalizerName)
		err := r.Update(context.Background(), userBinding)
		if err == nil {
			logUserBinding.Info(RemoveFinalizerSuccessMessage)
		}
		return err
	}
	return nil
}
