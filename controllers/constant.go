package controllers

import (
	"fmt"
	"time"
)

const (

	// resource kind
	ProjectKind     = "projects"
	UserBindingKind = "userbindings"
	ClusterKind     = "clusters"
	NodeKind        = "nodes"
	NamespaceKind   = "namespaces"

	// common finalizer name
	DefaultCustomFinalizerName   = "beastmaster.finalizers"
	DefaultStandardFinalizerName = "kubernetes"
	DefaultStopSyncMode          = false
	DefaultHostPort              = "127.0.0.1:8082"

	RemoveFinalizerSuccessMessage = "Remove Finalizers Success!"
)

var BaseUrl string
var ProjectUrl string
var UserBindingUrl string
var ClusterUrl string
var NodeUrl string
var NamespaceUrl string

var CustomFinalizerName string
var StandardFinalizerName string
var RequestTimeout time.Duration
var StopSyncMode bool
var LocateClusterName string
var HostPort string

func GetEnableControllers() string {
	return fmt.Sprintf("%s,%s,%s,%s,%s",
		ProjectKind, UserBindingKind, ClusterKind, NodeKind, NamespaceKind)
}

func InitParameter() {
	BaseUrl = fmt.Sprintf("http://%s/sync/", HostPort)
	ProjectUrl = BaseUrl + ProjectKind
	UserBindingUrl = BaseUrl + UserBindingKind
	ClusterUrl = BaseUrl + ClusterKind
	NodeUrl = BaseUrl + NodeKind
	NamespaceUrl = BaseUrl + NamespaceKind
}
