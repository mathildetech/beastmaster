package controllers

import (
	"k8s.io/apimachinery/pkg/types"
	clusterapi "k8s.io/cluster-registry/pkg/apis/clusterregistry/v1alpha1"
	"sigs.k8s.io/controller-runtime/pkg/log"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"net/http"
	"net/http/httptest"
)

var _ = Describe("ClusterController", func() {

	const timeout = time.Second * 30
	const interval = time.Second * 1
	var Cluster *clusterapi.Cluster

	BeforeEach(func() {

		logCluster = log.Log.WithValues("Cluster", "")

		spec := clusterapi.ClusterSpec{}

		key := types.NamespacedName{
			Name:      "cls-xp4lgkjq",
			Namespace: "alauda-system",
		}

		Cluster = &clusterapi.Cluster{
			ObjectMeta: metav1.ObjectMeta{
				Name:      key.Name,
				Namespace: key.Namespace,
				Labels: map[string]string{
					"federation.alauda.io/hostname": "cls-xp4lgkjq",
					"federation.alauda.io/name":     "jiapeng-test",
				},
				Annotations: map[string]string{
					"alauda.io/display-name": "ovn",
					"alauda.io/updated-at":   "2020-02-14T02:49:52Z",
				},
			},
			Spec: spec,
		}
	})

	Describe("send data to server", func() {
		Context("test response", func() {

			It("response statusCode should equal 200", func() {
				ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(200)
				}))
				defer ts.Close()

				httpCode, _ := SendClusterData(Cluster, http.MethodPut, ts.URL)
				Expect(httpCode).To(Equal(200))
			})

			It("response statusCode should equal 404", func() {
				ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(404)
				}))
				defer ts.Close()

				httpCode, _ := SendClusterData(Cluster, http.MethodPut, ts.URL)
				Expect(httpCode).To(Equal(404))
			})

			It("response statusCode should equal 500", func() {
				ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(500)
				}))
				defer ts.Close()

				httpCode, _ := SendClusterData(Cluster, http.MethodPut, ts.URL)
				Expect(httpCode).To(Equal(500))
			})
		})

		Context("tests different http method", func() {
			It("test with UPDATE method", func() {
				ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(200)
				}))
				defer ts.Close()

				httpCode, _ := SendClusterData(Cluster, http.MethodPut, ts.URL)
				Expect(httpCode).To(Equal(200))

			})

			It("test with DELETE method", func() {
				ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(200)
				}))
				defer ts.Close()

				httpCode, _ := SendClusterData(Cluster, http.MethodDelete, ts.URL)
				Expect(httpCode).To(Equal(200))

			})
		})

		Context("tests GetHttpMethod", func() {
			It("update event return PUT", func() {
				Expect(GetHttpMethod(false)).To(Equal(http.MethodPut))
			})

			It("create event return PUT", func() {
				Expect(GetHttpMethod(true)).To(Equal(http.MethodDelete))
			})

		})

	})

})
