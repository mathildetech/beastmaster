package controllers

import (
	authv1 "bitbucket.org/mathildetech/auth-controller2/pkg/apis/auth/v1"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/log"
	"time"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"net/http"
	"net/http/httptest"
)

var _ = Describe("UserBindingController", func() {

	const timeout = time.Second * 30
	const interval = time.Second * 1
	var UserBinding *authv1.UserBinding
	var key types.NamespacedName

	BeforeEach(func() {

		logUserBinding = log.Log.WithValues("UserBinding", "")

		spec := authv1.UserBindingSpec{}

		key = types.NamespacedName{
			Name:      "0a4b7b106f4b38aa9e98c0a65a852f91",
			Namespace: "default",
		}

		UserBinding = &authv1.UserBinding{
			ObjectMeta: metav1.ObjectMeta{
				Name:      key.Name,
				Namespace: key.Namespace,
				Labels: map[string]string{
					"alauda.io/cluster":                "",
					"alauda.io/namespace":              "",
					"alauda.io/project":                "acp-fed-jnshi",
					"auth.alauda.io/role.display-name": "2KAEbcqMR",
					"auth.alauda.io/role.level":        "project",
					"auth.alauda.io/role.name":         "acp-project-admin",
					"auth.alauda.io/user.email":        "7cf26bad66cfd2121956048307fd1cd0",
				},
				Annotations: map[string]string{
					"alauda.io/creator":                "admin@cpaas.io",
					"alauda.io/current-cluster":        "global",
					"auth.alauda.io/role.display-name": "项目管理员",
					"auth.alauda.io/user.email":        "jnpro@alauda.io",
				},
			},
			Spec: spec,
		}
	})

	Describe("send data to server", func() {
		Context("test response", func() {

			It("response statusCode should equal 200", func() {
				ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(200)
				}))
				defer ts.Close()

				httpCode, _ := SendUserBindingData(UserBinding, http.MethodPut, ts.URL)
				Expect(httpCode).To(Equal(200))
			})

			It("response statusCode should equal 404", func() {
				ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(404)
				}))
				defer ts.Close()

				httpCode, _ := SendUserBindingData(UserBinding, http.MethodPut, ts.URL)
				Expect(httpCode).To(Equal(404))
			})

			It("response statusCode should equal 500", func() {
				ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(500)
				}))
				defer ts.Close()

				httpCode, _ := SendUserBindingData(UserBinding, http.MethodPut, ts.URL)
				Expect(httpCode).To(Equal(500))
			})
		})

		Context("tests different http method", func() {
			It("test with UPDATE method", func() {
				ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(200)
				}))
				defer ts.Close()

				httpCode, _ := SendUserBindingData(UserBinding, http.MethodPut, ts.URL)
				Expect(httpCode).To(Equal(200))

			})

			It("test with DELETE method", func() {
				ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
					w.WriteHeader(200)
				}))
				defer ts.Close()

				httpCode, _ := SendUserBindingData(UserBinding, http.MethodDelete, ts.URL)
				Expect(httpCode).To(Equal(200))

			})
		})

		Context("tests GetHttpMethod", func() {
			It("update event return PUT", func() {
				Expect(GetHttpMethod(false)).To(Equal(http.MethodPut))
			})

			It("create event return PUT", func() {
				Expect(GetHttpMethod(true)).To(Equal(http.MethodDelete))
			})

		})

	})

})
