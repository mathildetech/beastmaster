package controllers

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	v1 "k8s.io/api/core/v1"
	"net/http"

	"github.com/go-logr/logr"
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

// NodeReconciler reconciles a Node object
type NodeReconciler struct {
	client.Client
	Log    logr.Logger
	Scheme *runtime.Scheme
}

type NodeReqBody struct {
	Node *v1.Node `json:"node"`
}

var logNode logr.Logger

// +kubebuilder:rbac:groups=auth.alauda.io,resources=Nodes,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=auth.alauda.io,resources=Nodes/status,verbs=get;update;patch

func (r *NodeReconciler) Reconcile(req ctrl.Request) (ctrl.Result, error) {
	_ = context.Background()
	logNode = r.Log.WithValues("Node", req.NamespacedName)

	// your logic here
	node := &v1.Node{}
	err := r.Get(context.Background(), req.NamespacedName, node)
	if err != nil {
		logNode.Error(err, "GetNode Error")
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	// StopSyncMode open, not sync resources
	if StopSyncMode {
		err := RemoveNodeFinalizer(r, node)
		return ctrl.Result{}, err
	}

	isDeleting := false
	if node.ObjectMeta.DeletionTimestamp.IsZero() {
		if !ContainsString(node.ObjectMeta.Finalizers, StandardFinalizerName) {
			node.ObjectMeta.Finalizers = append(node.ObjectMeta.Finalizers, StandardFinalizerName)
			err = r.Update(context.Background(), node)
			return ctrl.Result{}, err
		}
	} else {
		isDeleting = true
	}

	httpMethod := GetHttpMethod(isDeleting)
	statusCode, err := SendNodeData(node, httpMethod, NodeUrl)
	if err != nil {
		logNode.Error(err, "SendNodeData Error")
		return ctrl.Result{}, err
	}

	if statusCode >= http.StatusInternalServerError {
		logNode.Error(nil, "SendNodeData Error", "statusCode ", statusCode)
		return ctrl.Result{}, fmt.Errorf("internal error")
	}

	// remove finalizers for watched object if isDeleting equal true
	if isDeleting {
		err := RemoveNodeFinalizer(r, node)
		return ctrl.Result{}, err
	}

	return ctrl.Result{}, nil
}

func (r *NodeReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&v1.Node{}).Complete(r)
}

// send Node data to sync server
func SendNodeData(Node *v1.Node, httpMethod, url string) (httpCode int, err error) {
	NodeReqBody := NodeReqBody{Node: Node.DeepCopy()}
	body, _ := json.Marshal(NodeReqBody)
	req, err := http.NewRequest(httpMethod, url, bytes.NewBuffer(body))
	if err != nil {
		return 0, nil
	}
	logNode.Info("SendNodeData", "body:", string(body))
	requestId := uuid.New().String()
	req.Header.Set("request_id", requestId)
	req.Header.Set("Content-Type", "application/json")

	httpClient := &http.Client{Timeout: RequestTimeout}
	res, err := httpClient.Do(req)
	if err != nil {
		return 0, err
	}

	logNode.Info("SendNodeData",
		"StatusCode", res.StatusCode,
		"request_id", requestId,
		"httpMethod", httpMethod,
		"Node.name", Node.Name)

	return res.StatusCode, nil
}

// remove finalizers
func RemoveNodeFinalizer(r *NodeReconciler, node *v1.Node) error {
	if ContainsString(node.ObjectMeta.Finalizers, StandardFinalizerName) {
		node.ObjectMeta.Finalizers = removeString(node.ObjectMeta.Finalizers, StandardFinalizerName)
		err := r.Update(context.Background(), node)
		if err == nil {
			logNode.Info(RemoveFinalizerSuccessMessage)
		}
		return err
	}
	return nil
}
