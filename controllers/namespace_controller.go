/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"github.com/go-logr/logr"
	"github.com/google/uuid"
	"k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"net/http"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

// NamespaceReconciler reconciles a Namespace object
type NamespaceReconciler struct {
	client.Client
	Log    logr.Logger
	Scheme *runtime.Scheme
}

type NamespaceReqBody struct {
	Namespace *v1.Namespace `json:"namespace"`
}

var logNamespace logr.Logger

// +kubebuilder:rbac:groups=auth.alauda.io,resources=namespaces,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=auth.alauda.io,resources=namespaces/status,verbs=get;update;patch

func (r *NamespaceReconciler) Reconcile(req ctrl.Request) (ctrl.Result, error) {
	_ = context.Background()
	logNamespace = r.Log.WithValues("namespace", req.NamespacedName)

	// your logic here
	namespace := &v1.Namespace{}
	err := r.Get(context.Background(), req.NamespacedName, namespace)
	if err != nil {
		logNamespace.Error(err, "GetNamespace Error")
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	if StopSyncMode {
		err := RemoveNamespaceFinalizer(r, namespace)
		return ctrl.Result{}, err
	}

	isDeleting := false
	if namespace.ObjectMeta.DeletionTimestamp.IsZero() {
		if !ContainsString(namespace.ObjectMeta.Finalizers, StandardFinalizerName) {
			namespace.ObjectMeta.Finalizers = append(namespace.ObjectMeta.Finalizers, StandardFinalizerName)
			err = r.Update(context.Background(), namespace)
			return ctrl.Result{}, err
		}
	} else {
		isDeleting = true
	}

	httpMethod := GetHttpMethod(isDeleting)
	statusCode, err := SendNamespaceData(namespace, httpMethod, NamespaceUrl)
	if err != nil {
		logNamespace.Error(err, "SendNamespaceData Error")
		return ctrl.Result{}, err
	}

	if statusCode >= http.StatusInternalServerError {
		logNamespace.Error(nil, "SendNamespaceData Error", "statusCode:", statusCode)
		return ctrl.Result{}, fmt.Errorf("internal error")
	}

	// remove finalizers for watched object when isDeleting equal true
	if isDeleting {
		err := RemoveNamespaceFinalizer(r, namespace)
		return ctrl.Result{}, err

	}

	return ctrl.Result{}, nil
}

func (r *NamespaceReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&v1.Namespace{}).Complete(r)
}

// send Namespace data to sync server
func SendNamespaceData(namespace *v1.Namespace, httpMethod, url string) (httpCode int, err error) {
	namespaceReqBody := NamespaceReqBody{Namespace: namespace.DeepCopy()}
	body, _ := json.Marshal(namespaceReqBody)
	req, err := http.NewRequest(httpMethod, url, bytes.NewBuffer(body))
	if err != nil {
		return 0, nil
	}
	logNamespace.Info("SendNamespaceData", "body:", string(body))
	requestId := uuid.New().String()
	req.Header.Set("request_id", requestId)
	req.Header.Set("cluster", LocateClusterName)
	req.Header.Set("Content-Type", "application/json")

	httpClient := &http.Client{Timeout: RequestTimeout}
	res, err := httpClient.Do(req)
	if err != nil {
		return 0, err
	}

	logNamespace.Info("SendNamespaceData",
		"StatusCode", res.StatusCode,
		"request_id", requestId,
		"httpMethod", httpMethod,
		"namespace.name", namespace.Name)

	return res.StatusCode, nil
}

// remove finalizers
func RemoveNamespaceFinalizer(r *NamespaceReconciler, namespace *v1.Namespace) error {
	if ContainsString(namespace.ObjectMeta.Finalizers, StandardFinalizerName) {
		namespace.ObjectMeta.Finalizers = removeString(namespace.ObjectMeta.Finalizers, StandardFinalizerName)
		err := r.Update(context.Background(), namespace)
		if err == nil {
			logNamespace.Info(RemoveFinalizerSuccessMessage)
		}
		return err
	}
	return nil
}
