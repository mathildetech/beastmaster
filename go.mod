module bitbucket.org/mathildetech/beastmaster

go 1.13

require (
	bitbucket.org/mathildetech/auth-controller2 v1.8.1-0.20200106034749-4f2caa864f8b
	github.com/go-logr/logr v0.1.0
	github.com/google/uuid v1.1.1
	github.com/onsi/ginkgo v1.10.3
	github.com/onsi/gomega v1.7.1
	github.com/pkg/errors v0.8.1
	k8s.io/api v0.0.0-20190918155943-95b840bb6a1f
	k8s.io/apimachinery v0.0.1
	k8s.io/client-go v0.0.0-20190918160344-1fbdaa4c8d90
	k8s.io/cluster-registry v0.0.6
	sigs.k8s.io/controller-runtime v0.4.0
)

replace alauda.io/warpgate v0.0.1 => bitbucket.org/mathildetech/warpgate v0.0.1

replace k8s.io/apimachinery v0.0.1 => k8s.io/apimachinery v0.0.0-20191030190112-bb31b70367b7
